from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('home.urls')),
    path('', include('grupo.urls')),
    path('admin/', admin.site.urls),
    path('conta/', include('conta.urls')),
    path('<int:pk>/', include('subgrupo.urls')),
    path('<int:pk>/<int:pk>/', include('tarefa.urls')),
]
