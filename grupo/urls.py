from django.urls import path, include
from .views import VerGrupo, CriarGrupo, AtualizarGrupo, ApagarGrupo
from django.contrib import admin

app_name = 'grupo'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('grupo/criar/', CriarGrupo.as_view(), name='criar'),
    path('<int:pk>/', VerGrupo.as_view(), name='grupo'),
    path('<int:pk>/atualizar/', AtualizarGrupo.as_view(), name='atualizar'),
    path('<int:pk>/apagar/', ApagarGrupo.as_view(), name='apagar'),
]
